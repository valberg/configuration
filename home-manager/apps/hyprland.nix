{pkgs, ...}:
{
    wayland.windowManager.hyprland = {
      enable = true;
      systemd.enable = true;
      package = pkgs.hyprland;

      settings = {
        "$terminal" = "foot";
        "$mod" = "SUPER";
        monitor=",preferred,auto,1.5";
        input = {
          touchpad = {
            natural_scroll = true;
          };
        };
        bind = [
          "$mod, Q, exec, $terminal"
          "$mod, F, exec, firefox"
        ];

      };
    };
}
