{ config, pkgs, ... }:

{
  programs.home-manager.enable = true;
  home.username = "valberg";
  home.homeDirectory = "/home/valberg";
  home.stateVersion = "23.11";
  home.sessionVariables = {
    FLAKE = "/home/valberg/configuration/";
  };

  nixpkgs.config.allowUnfree = true;

  home.packages = with pkgs; [

# Python stuff
    python311Packages.tox
    python311Packages.virtualenv
    pipx
    hatch
    ruff
    jetbrains.pycharm-professional
    pyenv

# Chats
    signal-desktop
    element-desktop
    discord
    slack

# Terminals
    blackbox-terminal
    foot

# Commandline
    nerdfonts

# Development
    pre-commit

# Ansible
    ansible

# Gnome
    gnomeExtensions.forge
    gnome.gnome-tweaks

# Mail
    thunderbird

# Passwords
    keepassxc

# Backup
    deja-dup
    synology-drive-client

# Note taking
    obsidian

# Music
    tidal-hifi

# Nix
    nh
  ];

  imports = [
    ./apps/hyprland.nix
  ];

  programs.fish = {
    enable = true;
    shellAbbrs = {
      update-home = "nh home switch --nom";
      update-os = "nh os switch --nom";
    };
  };

  programs.atuin = {
    enable = true;
    enableFishIntegration = true;
  };

  programs.starship = {
    enable = true;
    enableFishIntegration = true;
  };

  programs.zoxide = {
    enable = true;
    enableFishIntegration = true;
  };

  programs.tmux = {
    enable = true;
    clock24 = true;
    keyMode = "vi";
    prefix = "C-a";
    customPaneNavigationAndResize = true;
    extraConfig = ''
    '';
  };

  programs.fzf = {
    enable = true;
    enableFishIntegration = true;
    tmux.enableShellIntegration = true;
  };

  programs.broot = {
    enable = true;
    enableFishIntegration = true;
    modal = true;
  };

  programs.btop = {
    enable = true;
  };

  programs.bat = {
    enable = true;
  };

  programs.firefox = {
    enable = true;
    enableGnomeExtensions = true;
  };

  programs.ripgrep = {
    enable = true;
  };

}
